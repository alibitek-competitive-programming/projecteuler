#!/usr/bin/env python

import unittest
import math

def sum_multiples_of_3_and_5(n):
  """
    Problem 1: Multiples of 3 and 5
    If we list all the natural numbers below 10 that are multiples of 3 or 5 we get 3, 5, 6, 9.
    The sum of these multiples is 23
    
    Find the sum of all the multiples of 3 or 5 below the provided parameter value number.
  """
  total = 0
  n -= 1
  while n:
    if n % 3 == 0 or n % 5 == 0:
      total += n
    n -= 1    
  return total
 

class SumMultiplesOf3And5TestCase(unittest.TestCase):
  def test_sum_multiples_of_3_and_5(self):
    test_cases = [ (10,23), (49, 543), (1000, 233168), (8456,16687353), (19564,89301183) ]
    for index, test_case in enumerate(test_cases):
      want = test_case[1]
      got = sum_multiples_of_3_and_5(test_case[0])
      with self.subTest(i=index, n=test_case[0]):
        self.assertEqual(want, got)
  
if __name__ == '__main__':
  unittest.main()
 
