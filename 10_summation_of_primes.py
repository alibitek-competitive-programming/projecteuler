#!/usr/bin/env python

import unittest
import math

def is_prime(n):
  if n == 2:
    return True
  
  if n < 2 or n % 2 == 0:
    return False
  
  for i in range(3, int(math.sqrt(n))+1, 2):
    if n % i == 0:
      return False
    
  return True

def sum_of_primes(n):
  """
    Problem 10: The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17
    Find the sum of all the primes below n.
  """
  if n < 2:
    return 0
  
  total = 2
  
  number = 3
  while number < n:
    if is_prime(number):
      total += number
    number += 2
  
  return total
 

class SumOfPrimesTestCase(unittest.TestCase):
  def test_sum_of_primes(self):
    test_cases = [ (17,41), (2001,277050), (140759, 873608362), (2000000,142913828922)]
    for index, test_case in enumerate(test_cases):
      want = test_case[1]
      got = sum_of_primes(test_case[0])
      with self.subTest(i=index, n=test_case[0]):
        self.assertEqual(want, got)
  
if __name__ == '__main__':
  unittest.main()
 
