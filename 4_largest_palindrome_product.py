#!/usr/bin/env python

import unittest
import math

def is_palindrome(n):
  return n == int(str(n)[::-1])

def largest_palindrome_product(n):
  """
    Problem 4: Largest palindrome product
    A palindromic number reads the same both ways.
    The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 * 99
    Find the largest palindrome made from the product of two n-digit numbers.
  """
  largest = 0
  minNumber = int("1" + ''.zfill(n-1))
  maxNumber = int("".rjust(n, '9'))
  for i in range(maxNumber, minNumber + 1, -1):
    for j in range(maxNumber, minNumber + 1, -1):
      product = i * j
      if is_palindrome(product) and product > largest:
        largest = product
  return largest


class LargestPalindromeProductTestCase(unittest.TestCase):
  def test_is_palindrome(self):
    test_cases = [ (1010, False), (1111,True), (9009, True) ]
    for index, test_case in enumerate(test_cases):
      want = test_case[1]
      got = is_palindrome(test_case[0])
      with self.subTest(i=index, n=test_case[0]):
        self.assertEqual(want, got)
        
  def test_largest_palindrome_product(self):
    test_cases = [ (2,9009), (3,906609) ]
    for index, test_case in enumerate(test_cases):
      want = test_case[1]
      got = largest_palindrome_product(test_case[0])
      with self.subTest(i=index, n=test_case[0]):
        self.assertEqual(want, got)
  
if __name__ == '__main__':
  unittest.main()
 
