#!/usr/bin/env python

import time

def smallest_multiple(n):
    """
      Problem 5: Smallest multiple
      2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any reminder
      
      What is the smallest positive number that is evenly divisible by all of the numbers from 1 to n ?
    """
    start_time = time.time()
    smallest_mult = n
    
    while True:      
      if has_all_valid_divisors(smallest_mult, n):        
        print("Found", smallest_mult, n)
        break
      else:
        smallest_mult += 1
      
    elapsed_time = time.time() - start_time
    print("Elapsed time for smallest_multiple({0})={1}".format(n, elapsed_time))
    return smallest_mult


def has_all_valid_divisors(number, n):
  for i in range(1, n + 1):
    if number % i != 0:
      return False
  return True

if __name__ == '__main__':
  test_cases = [ (5,60), (7, 420), (10,2520), (13,360360), (20,232792560) ]
  for test_case in test_cases:    
    want = test_case[1]
    got = smallest_multiple(test_case[0])
    print("Checking test case: {0} - want: {1}, got: {2}\n".format(test_case, want, got))
    assert want == got
    
"""
Found 60 5
Checking test case: (5, 60) - want: 60, got: 60

Found 420 7
Checking test case: (7, 420) - want: 420, got: 420

Found 2520 10
Checking test case: (10, 2520) - want: 2520, got: 2520

Found 360360 13
Checking test case: (13, 360360) - want: 360360, got: 360360

Found 232792560 20
Checking test case: (20, 232792560) - want: 232792560, got: 232792560
"""
