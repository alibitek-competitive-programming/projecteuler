#!/usr/bin/env python

def sum_square_difference(n):
  """
    Problem 6: Sum square difference
  """
  sum_of_the_squares = 0
  square_of_the_sum = 0
  
  # Optimization: 
  # sum_of_numbers = (n * (n + 1)) / 2
  # sum_of_squares = (n * (n + 1) * (2 * n + 1)) / 6
  
  for num in range(1, n + 1):
    sum_of_the_squares += (num ** 2)
    square_of_the_sum += num

  square_of_the_sum = square_of_the_sum ** 2 
  
  return abs(sum_of_the_squares - square_of_the_sum)

if __name__ == '__main__':
  test_cases = [ (10,2640), (20, 41230), (100, 25164150) ]
  for test_case in test_cases:
    want = test_case[1]
    got = sum_square_difference(test_case[0])
    print("Checking sum_square_difference({0}); want: {1}, got: {2}".format(test_case[0], want, got))
    assert want == got
