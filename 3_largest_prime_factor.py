#!/usr/bin/env python

import unittest
import math

def is_prime(n):
  if n == 2:
    return True
  
  if n < 2 or n % 2 == 0:
    return False
  
  for i in range(3, int(math.sqrt(n))+1, 2):
    if n % i == 0:
      return False
    
  return True

def largest_prime_factor(n):
  """
    Problem 3: Largest prime factor
    The prime factors of 13195 are 5, 7, 13 and 29
    What is the largest prime factor of the given number?
  """       
  if n < 2:
    return 1  
  
  if is_prime(n):
    return n
  
  sqrt = math.floor(math.sqrt(n))
  if sqrt % 2 == 0:
    sqrt += 1
  
  for i in range(sqrt, 0, -2):
    if n % i == 0 and is_prime(i):
      return i
  
  return 1

class LargestPrimeFactorTestCase(unittest.TestCase):
  def test_largest_prime_factor(self):
    test_cases = [ (2,2), (3,3), (5,5), (7,7), (13195,29), (600851475143,6857) ]
    for index, test_case in enumerate(test_cases):
      want = test_case[1]
      got = largest_prime_factor(test_case[0])
      with self.subTest(i=index, n=test_case[0]):
        self.assertEqual(want, got)
  
if __name__ == '__main__':
  unittest.main()
 
