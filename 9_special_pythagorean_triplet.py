#!/usr/bin/env python

import unittest
import math

def is_triplet(a, b, c):
  return a**2 + b**2 == c**2

def special_pythagorean_triplet(n):
  """
    A Pythagorean triplet is a set of three natural numbers, a < b < c, for which a^2 + b^2 = c^2
    
    For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2
    
    There exists exactly one Pythagorean triplet for which a + b + c = 1000.
    Find the product abc such that a + b + c = n
  """
  for a in range(1, (n // 3) - 1):
    for b in range(a + 1, (n // 2) - 1):
      c = n - a - b
      if is_triplet(a, b, c) and (a + b + c == n):
        return a * b * c      
  return 1

class TestSpecialPythagoreanTripletTestCase(unittest.TestCase):
  def test_special_pythagorean_triplet(self):
    test_cases = [ (24,480), (120,49920), (1000, 31875000) ]
    for index, test_case in enumerate(test_cases):
      want = test_case[1]
      got = special_pythagorean_triplet(test_case[0])
      with self.subTest(i=index, n=test_case[0]):
        self.assertEqual(want, got)
  
if __name__ == '__main__':
  unittest.main()
