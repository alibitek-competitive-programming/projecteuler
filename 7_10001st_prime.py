#!/usr/bin/env python

import unittest
import math

def is_prime(n):
  """
    returns true if the number is divisible by 1 and by itself
  """
  if n == 2:
    return True
  
  if n < 2 or (n % 2 == 0):
    return False  
  
  for i in range(3, int(math.sqrt(n))+1, 2):
    if n % i == 0:
      return False
    
  return True

def nth_prime(n):
  """
    Problem 7: 10001st prime
    By listing the first six prime numbers: 2, 3, 5, 7, 11 and 13, we can see that they 6th prime is 13
    What is the nth prime number?
  """
  primes = [2]
  number = 3

  while (len(primes) < n):
    if is_prime(number):
      primes.append(number)
    number += 2

  return primes[-1]

class TestPrimesTestCase(unittest.TestCase):
  
  def test_is_prime(self):
    test_cases = [ (1, False), (2, True), (3, True), (4, False), (5, True), (7, True), (12, False), (13, True) ]
    for test_case in test_cases:
      want = test_case[1]      
      got = is_prime(test_case[0])
      print("Checking is_prime({0}), want: {1}, got: {2}".format(test_case[0], want, got))
      self.assertEqual(want, got)
  
  def test_nth_prime(self):
    print("")
    test_cases = [ (6, 13), (10, 29), (100, 541), (1000, 7919), (10001, 104743) ]
    for test_case in test_cases:
      want = test_case[1]
      got = nth_prime(test_case[0])
      print("Checking nth_prime({0}), want: {1}, got: {2}".format(test_case[0], want, got))
      self.assertEqual(want, got)
    
if __name__ == '__main__':
    unittest.main()
